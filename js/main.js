$(document).ready(function() {

	$('.main-slider').slick({
		autoPlay: true,
		prevArrow: '<a class="arrow arrow-left" href="#">‹</a>',
		nextArrow:'<a class="arrow arrow-right" href="#">›</a>',
	});

	$('.to-top').click(function() {
		$('html, body').animate({
			scrollTop: 0
		}, 700);
		return false;
	});

});